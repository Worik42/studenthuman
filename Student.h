//
// Created by worik on 21.12.2017.
//

#ifndef STUDENTHUMAN_STUDENT_H
#define STUDENTHUMAN_STUDENT_H


#include "Human.h"

class Student : public Human {
private:
    int yearOfAdmission;
    int ageLearn;
public:
    Student() : Human() {}

    int getYearOfAdmission();

    void setYearOfAdmission(int yearOfAdmission);

    int getAgeLearn();

    virtual void show();
};


#endif //STUDENTHUMAN_STUDENT_H
