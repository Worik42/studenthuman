//
// Created by worik on 21.12.2017.
//

#ifndef STUDENTHUMAN_HUMAN_H
#define STUDENTHUMAN_HUMAN_H

#include <string>

using namespace std;

class Human {
private:
    string name = "";
    string lastName = "";
    int year;
    int age;
public:

    Human();

    const string &getName() const;

    void setName(const string &name);

    const string &getLastName() const;

    void setLastName(const string &lastName);

    int getYear() const;

    void setYear(int year);

    int getAge();

    virtual void show();
};


#endif //STUDENTHUMAN_HUMAN_H
