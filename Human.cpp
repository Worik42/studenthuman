//
// Created by worik on 21.12.2017.
//

#include <iostream>
#include "Human.h"

Human::Human() {
}

const string &Human::getName() const {
    return name;
}

void Human::setName(const string &name) {
    Human::name = name;
}

const string &Human::getLastName() const {
    return lastName;
}

void Human::setLastName(const string &lastName) {
    Human::lastName = lastName;
}

int Human::getYear() const {
    return year;
}

void Human::setYear(int year) {
    Human::year = year;
    age = 2017 - year;
}

int Human::getAge() {

    return age;
}

void Human::show() {
    cout << "Name:" << name << endl;
    cout << "Last Name:" << lastName << endl;
    cout << "Was born:" << year << endl;
    cout << "Current age:" << age << endl;
}
