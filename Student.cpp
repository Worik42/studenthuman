//
// Created by worik on 21.12.2017.
//

#include <iostream>
#include "Student.h"

int Student::getAgeLearn() {
    return ageLearn;
}

void Student::setYearOfAdmission(int yearOfAdmission) {
    Student::yearOfAdmission = yearOfAdmission;
    ageLearn = 2017 - yearOfAdmission;
}

int Student::getYearOfAdmission() {
    return yearOfAdmission;
}

void Student::show() {
    cout << "Year Of Admission" << yearOfAdmission << endl;
}
