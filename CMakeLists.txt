cmake_minimum_required(VERSION 3.9)
project(studenthuman)

set(CMAKE_CXX_STANDARD 14)

add_executable(studenthuman main.cpp Human.cpp Human.h Student.cpp Student.h)