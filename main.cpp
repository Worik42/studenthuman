#include <iostream>
#include "Human.h"
#include "Student.h"
#include <list>

int main() {
    list<Human> humans;
    Student s;
    s.setName("Nikola");
    s.setLastName("Piterskiy");
    s.setYear(1998);
    s.setYearOfAdmission(2016);

    Student s1;
    s1.setName("Djenya");
    s1.setLastName("Gurov");
    s1.setYear(1998);
    s1.setYearOfAdmission(2015);

    Human s2;
    s2.setName("QQ");
    s2.setLastName("QQQQQQ");
    s2.setYear(1908);
    humans.push_front(s);
    humans.push_front(s1);
    humans.push_front(s2);
    for (list<Human>::iterator it = humans.begin(); it != humans.end(); it++)
        it->show();

    return 0;
}
